package com.atguigu.cloudalibaba.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: my.seaTide
 * @create: 2021/12/4 3:00 下午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Storage {
    private Integer id;
    private Long productId;
    private Integer total;
    private Integer used;
    private Integer residue;
}
