package com.atguigu.cloudalibaba.service;

/**
 * @author: my.seaTide
 * @create: 2021/12/4 3:08 下午
 */
public interface StorageService {

    /**
     * 扣减库存
     *
     * @param productId
     * @param count
     */
    void decrease(Long productId, Integer count);
}
