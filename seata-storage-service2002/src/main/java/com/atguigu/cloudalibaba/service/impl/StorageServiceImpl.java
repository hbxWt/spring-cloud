package com.atguigu.cloudalibaba.service.impl;

import com.atguigu.cloudalibaba.dao.StorageDao;
import com.atguigu.cloudalibaba.service.StorageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: my.seaTide
 * @create: 2021/12/4 3:09 下午
 */
@Service
public class StorageServiceImpl implements StorageService {

    @Resource
    private StorageDao storageDao;

    /**
     * 扣减库存
     *
     * @param productId
     * @param count
     */
    @Override
    public void decrease(Long productId, Integer count) {
        storageDao.decrease(productId, count);
    }
}
