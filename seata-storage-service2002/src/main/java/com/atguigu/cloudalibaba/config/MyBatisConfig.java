package com.atguigu.cloudalibaba.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author: my.seaTide
 * @create: 2021/12/4 3:13 下午
 */
@Configuration
@MapperScan({"com.atguigu.cloudalibaba.dao"})
public class MyBatisConfig {
}
