package com.atguigu.cloudalibaba.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author: my.seaTide
 * @create: 2021/12/2 10:27 下午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    private Integer id;
    private Long userId;
    private Long productId;
    private Integer count;
    private BigDecimal money;
    private Integer status;
}
