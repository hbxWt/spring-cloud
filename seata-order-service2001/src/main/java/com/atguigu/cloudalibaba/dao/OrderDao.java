package com.atguigu.cloudalibaba.dao;

import com.atguigu.cloudalibaba.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author: my.seaTide
 * @create: 2021/12/2 10:32 下午
 */
@Mapper
public interface OrderDao {

    /**
     * 创建订单
     *
     * @param order
     */
    void createOrder(Order order);

    /**
     * 修改订单状态
     *
     * @param id
     * @param status
     */
    void update(@Param("userId") Long userId, @Param("status") Integer status);
}
