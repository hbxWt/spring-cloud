package com.atguigu.cloudalibaba.service;

import com.atguigu.cloudalibaba.domain.Order;

/**
 * @author: my.seaTide
 * @create: 2021/12/2 10:47 下午
 */
public interface OrderService {

    /**
     * 创建订单
     *
     * @param order
     */
    void create(Order order);
}
