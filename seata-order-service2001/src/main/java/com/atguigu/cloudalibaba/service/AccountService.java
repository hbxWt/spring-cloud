package com.atguigu.cloudalibaba.service;

import com.atguigu.cloudalibaba.domain.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author: my.seaTide
 * @create: 2021/12/2 10:53 下午
 */
@FeignClient("seata-account-service")
public interface AccountService {

    /**
     * 扣减余额
     *
     * @param userId
     * @param money
     * @return
     */
    @PostMapping("/account/decrease")
    CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);

}
