package com.atguigu.cloudalibaba.service.impl;

import com.atguigu.cloudalibaba.dao.OrderDao;
import com.atguigu.cloudalibaba.domain.Order;
import com.atguigu.cloudalibaba.service.AccountService;
import com.atguigu.cloudalibaba.service.OrderService;
import com.atguigu.cloudalibaba.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: my.seaTide
 * @create: 2021/12/2 10:48 下午
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDao orderDao;

    @Resource
    private StorageService storageService;

    @Resource
    private AccountService accountService;

    @Override
    @GlobalTransactional
    public void create(Order order) {

            log.info("---订单开始创建");
            // 创建订单
            orderDao.createOrder(order);

            // 扣减库存
            log.info("扣减库存开始---");
            storageService.decrease(order.getProductId(), order.getCount());
            log.info("扣减库存结束---");

            // 扣减用户余额
            log.info("扣减余额开始---");
            accountService.decrease(order.getUserId(), order.getMoney());
            log.info("扣减余额结束---");

            // 更改订单状态
            log.info("更改订单状态开始---");
            orderDao.update(order.getUserId(), 1);
            log.info("更改订单状态结束---");

            log.info("下单结束---");
    }
}
