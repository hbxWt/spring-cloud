package com.atguigu.springcloud.service;

/**
 * @author: my.seaTide
 * @create: 2021/11/24 11:53 下午
 */
public interface IMessageProvider {
    public String send();
}
