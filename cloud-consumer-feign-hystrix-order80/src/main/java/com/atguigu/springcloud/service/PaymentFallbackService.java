package com.atguigu.springcloud.service;

import org.springframework.stereotype.Component;

/**
 * @author: my.seaTide
 * @create: 2021/11/22 9:30 下午
 */
// 第三种方式：统一为接口里面的方法进行异常处理
@Component
public class PaymentFallbackService implements PaymentService{

    @Override
    public String paymentInfo_ok(Integer id) {
        return "服务调用失败-paymentInfo_ok：" + id;
    }

    @Override
    public String paymentInfo_Timeout(Integer id) {
        return "服务调用失败-paymentInfo_Timeout：" + id;
    }
}
