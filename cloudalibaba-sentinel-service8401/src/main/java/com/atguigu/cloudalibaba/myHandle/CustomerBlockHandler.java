package com.atguigu.cloudalibaba.myHandle;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.atguigu.springcloud.entities.CommonResult;

/**
 * @author: my.seaTide
 * @create: 2021/11/30 10:16 下午
 */
public class CustomerBlockHandler {
    public static CommonResult handleException(BlockException exception){
        return new CommonResult( 2020 , " 自定义的限流处理信息 ......CustomerBlockHandler" , null);
    }

    public static CommonResult handleException1(BlockException exception){
        return new CommonResult( 2020 , " 自定义的限流处理信息 ......CustomerBlockHandler1" , null);
    }
}
