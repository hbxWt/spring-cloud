package com.atguigu.cloudalibaba.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author: my.seaTide
 * @create: 2021/11/29 9:41 下午
 */
@RestController
@Slf4j
public class FlowLimitController {

    @GetMapping("/testA")
    public String testA() {
        return "------testA";
    }

    @GetMapping("/testB")
    public String testB() {
        log.info(Thread.currentThread().getName() + "\t" + "...testB");
        return "------testB";
    }

    @GetMapping ( "/testD" )
    public String testD(){
        // 暂停几秒钟线程
//        try { TimeUnit. SECONDS .sleep( 1 ); } catch (InterruptedException e) { e.printStackTrace(); }
        log .info( "testD 测试 RT" );

        int num = 10 / 0;

        return "------testD" ;
    }

    @GetMapping("/testHotKey")
    @SentinelResource(value = "testHotKey", blockHandler = "dealHandler_testHotKey")
    public String testHotKey(@RequestParam(value = "p1",required = false) String p1,
                             @RequestParam(value = "p2", required = false) String p2) {


        return "------testHotKey,p1=" + p1 + ",p2=" + p2;
    }

    public String dealHandler_testHotKey(String p1, String p2, BlockException blockException) {
        return "-----dealHandler_testHotKey";
    }
}
