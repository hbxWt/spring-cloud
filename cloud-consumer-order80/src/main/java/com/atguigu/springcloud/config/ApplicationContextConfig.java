package com.atguigu.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author: my.seaTide
 * @create: 2021/11/15 10:00 下午
 */
@Configuration
@LoadBalancerClient(name = "CLOUD-PAYMENT-SERVICE", configuration = MySelfRule.class)
public class ApplicationContextConfig {

    // 负载均衡
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
