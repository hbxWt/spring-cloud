package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import com.atguigu.springcloud.service.PaymentService;
import com.netflix.appinfo.InstanceInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author: my.seaTide
 * @create: 2021/11/14 2:57 下午
 */
@Slf4j
@RestController
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private Integer port;

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentId(@PathVariable("id") Long id) {

        Payment info = paymentService.getPaymentById(id);

        if (info != null) {
            return new CommonResult(200, "查询成功，端口号：" + port, info);
        } else {
            return new CommonResult(444, "没有对应记录", null);
        }
    }

    @PostMapping("/payment/create")
    public CommonResult<Payment> create(@RequestBody Payment payment) {
        int rows = paymentService.create(payment);

        if(rows > 0) {
            return new CommonResult(200, "添加成功", rows);
        } else {
            return new CommonResult(444, "添加失败", null);
        }
    }

    @GetMapping("/payment/discovery")
    public Object discovery() {

        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for ( ServiceInstance instance: instances) {
            System.out.println(instance.getHost() + "-" + instance.getPort() + '-' + instance.getInstanceId());
        }

        return this.discoveryClient;
    }

    @GetMapping("/payment/feign/timeout")
    public Integer paymentFeignTimeOut() {
        System.out.println( "*****paymentFeignTimeOut from port: " + port );
        // 暂停几秒钟线程
        try { TimeUnit. SECONDS .sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
        return port;
    }

    @GetMapping("/payment/lb/{id}")
    public Integer paymentLb(@PathVariable("id") Long id) {
        return port;
    }

}
