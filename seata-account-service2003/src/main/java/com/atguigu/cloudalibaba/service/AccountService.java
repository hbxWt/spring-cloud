package com.atguigu.cloudalibaba.service;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * @author: my.seaTide
 * @create: 2021/12/4 3:27 下午
 */
public interface AccountService {

    /**
     * 扣减余额
     *
     * @param userId
     * @param money
     */
    void decrease(@Param("userId") Long userId, @Param("money")BigDecimal money);
}
