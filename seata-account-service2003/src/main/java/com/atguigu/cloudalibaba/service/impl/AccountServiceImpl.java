package com.atguigu.cloudalibaba.service.impl;

import com.atguigu.cloudalibaba.dao.AccountDao;
import com.atguigu.cloudalibaba.service.AccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author: my.seaTide
 * @create: 2021/12/4 3:29 下午
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountDao accountDao;

    @Override
    public void decrease(Long userId, BigDecimal money) {
        accountDao.decrease(userId, money);
    }
}
