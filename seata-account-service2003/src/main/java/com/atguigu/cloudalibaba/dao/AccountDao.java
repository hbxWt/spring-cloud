package com.atguigu.cloudalibaba.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * @author: my.seaTide
 * @create: 2021/12/4 3:22 下午
 */
@Mapper
public interface AccountDao {

    /**
     * 扣减余额
     *
     * @param userId
     * @param money
     */
    void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);

}
