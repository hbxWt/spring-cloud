# spring-cloud

#### 介绍
spring-cloud相关组件的使用


#### 软件架构
- 服务注册：eureka、consul、nacos
- 服务调用：ribbon+RestTemplate、openFeign
- 网关：gateway
- 消息中间件：stream+rabbitMQ
- 服务降级、熔断、限流：hystrix、sentinel
- 参数配置：spring-config + bus、nacos+config
- 分布式事务：seata