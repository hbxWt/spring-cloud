package com.atguigu.cloudalibaba.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.atguigu.cloudalibaba.service.PaymentService;
import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author: my.seaTide
 * @create: 2021/11/30 10:47 下午
 */
@RestController
public class CircleBreakerController {

    @Value("${service-url.nacos-user-service}")
    private String serviceUrl;

//    @Resource
//    private RestTemplate restTemplate;

//    @RequestMapping("/consumer/fallback/{id}")
//    @SentinelResource(value = "fallback", fallback = "handlerFallback") fallback只负责业务异常
//    @SentinelResource(value = "fallback", blockHandler = "blockHandler") // blockHandler只负责sentinel控制台配置违规
//    @SentinelResource(value = "fallback", blockHandler = "blockHandler", fallback = "handlerFallback",
//            exceptionsToIgnore = {IllegalArgumentException. class })// exceptionsToIgnore 忽略属性
//    public CommonResult<Payment> fallback(@PathVariable("id") Long id) {
//        CommonResult<Payment> commonResult = restTemplate.getForObject(serviceUrl + "/paymentSQL/" + id, CommonResult.class, id);
//
//        if (id == 4) {
//            throw new IllegalArgumentException ( "IllegalArgumentException, 非法参数异常 ...." );
//        } else if (commonResult.getData() == null) {
//            throw new NullPointerException ( "NullPointerException, 该 ID 没有对应记录 , 空指针异常 " );
//        }
//
//        return commonResult;
//    }

    public CommonResult handlerFallback(@PathVariable("id") Long id, Throwable e) {
        Payment payment = new Payment(id, null);
        return new CommonResult(444, "fallback 无此流水, exception:" + e.getMessage(), payment);
    }

    public CommonResult blockHandler(@PathVariable("id") Long id, BlockException exception) {
        Payment payment = new Payment(id, null);
        return new CommonResult(444, "blockHandler-sentinel 限流 , 无此流水 : blockException, exception:" + exception.getMessage(), payment);
    }



    // ===============openfeign================

    @Resource
    private PaymentService paymentService;

    @GetMapping(value = "/consumer/paymentSQL/{id}" )
    @SentinelResource(value = "fallback", fallback = "handlerFallback") // fallback只负责业务异常
    public CommonResult<Payment> paymentSQL(@PathVariable( "id" ) Long id) {
        if (id == 4) {
            throw new RuntimeException( " 没有该 id" );
        }
        return paymentService .paymentSQL(id);
    }

}
