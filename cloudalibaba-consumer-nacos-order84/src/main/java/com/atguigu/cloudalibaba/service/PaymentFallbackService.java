package com.atguigu.cloudalibaba.service;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import org.springframework.stereotype.Component;

/**
 * @author: my.seaTide
 * @create: 2021/12/1 12:02 上午
 */
@Component
public class PaymentFallbackService implements PaymentService{
    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<>( 444 , " 服务降级返回 , 没有该流水信息 " , new Payment(id, "errorSerial......" ));
    }
}
