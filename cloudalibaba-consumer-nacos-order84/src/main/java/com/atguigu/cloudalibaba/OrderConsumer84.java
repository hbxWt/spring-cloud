package com.atguigu.cloudalibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author: my.seaTide
 * @create: 2021/11/30 10:43 下午
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients  // 添加对feign的支持
public class OrderConsumer84 {
    public static void main(String[] args) {
        SpringApplication.run(OrderConsumer84.class, args);
    }
}
