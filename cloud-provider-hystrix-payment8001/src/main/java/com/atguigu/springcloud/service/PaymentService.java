package com.atguigu.springcloud.service;

import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.TimeUnit;

/**
 * @author: my.seaTide
 * @create: 2021/11/20 9:03 下午
 */
@Service
public class PaymentService {

    /**
     * 正常访问，一切ok
     *
     * @param id
     * @return
     */
    public String paymentInfo_OK(Integer id) {
        return " 线程池 :" +Thread. currentThread ().getName()+ "，paymentInfo_OK,id: " +id+ " \t " + "O(∩_∩)O" ;
    }

    /**
     * 超时访问，演示降级
     *
     * @param id
     * @return
     */
    @HystrixCommand(fallbackMethod = "paymentInfo_TimeoutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
    })
    public String paymentInfo_Timeout(Integer id) {

//        int age = 10 / 0;
        int second = 2;

        try { TimeUnit. SECONDS .sleep( second ); } catch (InterruptedException e) { e.printStackTrace(); }
        return " 线程池 :" +Thread. currentThread ().getName()+ "，paymentInfo_Timeout,id: " +id+ " \t " + "O(∩_∩)O，耗费" + second + "秒" ;
    }

    public String paymentInfo_TimeoutHandler(Integer id) {
        return "调用支付接口超时或异常，当前线程池名字：" + Thread.currentThread().getName();
    }

    //========= 服务熔断
    @HystrixCommand (fallbackMethod = "paymentCircuitBreaker_fallback" ,commandProperties = {
            @HystrixProperty (name = "circuitBreaker.enabled" ,value = "true" ),
            @HystrixProperty (name = "circuitBreaker.requestVolumeThreshold" ,value = "10" ),
            @HystrixProperty (name = "circuitBreaker.sleepWindowInMilliseconds" ,value = "10000" ),
            @HystrixProperty (name = "circuitBreaker.errorThresholdPercentage" ,value = "60" ), })
    public String paymentCircuitBreaker( @PathVariable( "id" ) Integer id) {
        if (id < 0 ) {
            throw new RuntimeException( "******id 不能负数 " );
        }
        String serialNumber = IdUtil. simpleUUID ();
        return Thread. currentThread ().getName()+ " \t " + " 调用成功，流水号 : " + serialNumber;
    }

    public String paymentCircuitBreaker_fallback(@PathVariable ( "id" ) Integer id){
        return "id 不能负数，请稍后再试， /( ㄒ o ㄒ )/~~  id: " +id;
    }
}
